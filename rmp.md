# Raamatupidamise liides

## Printsiibid

1. On **kauba saaja**, Opiqus on kauba saajaks kliendikonto, st kas kool/juriidiline isik või eraisikust klient. Ostetud litsentsid saavad eranditult kliendikonto kasutajate kasutada Eraisiku kliendikonto on eraisiku nimeline, tekib koos kasutajaga, st tulevasel kasutajal lastakse sisestada enda ees- ja perekonnanimi ning selle järgi nimetatakse ka kliendikonto. 
1. On **arve saaja**, Opiqus tellimuse kinnitamise lehel "arve saaja", maksja võib olla nii eraisik, kui füüsiline isik ja seda nii koolist, kui eraisikust tellija puhul.
1. Peaks tellimuste saamisel kindlaks tegema **arve saaja** asukohamaa ja VAT numbri, mis on vajalikus edasises tehingu maksustamise määratlemisel, asukohamaa tuleb kindlaks teha nii juriidilise kui füüsilise isiku puhul:
    1. Kui arve saaja asukoht on Eesti, siis rakendada universaalselt Eesti kohalikku käibemaksumäära (Opiqu puhul 9%) sõltumata sellest kas arve saaja on registreeritud käibemaksukohustuslasena või mitte ja kas ta on juriidiline või füüsiline isik.
    1. Kui arve saaja asukoht on väljaspool Eestit, aga mõnes Euroopa Liidu liikmesriigis, siis peab kindlaks tegema, kas arve saaja on registreeritud käibemaksukohustuslane - peame küsima ja tuleb kontrollida esitatud VAT numbrit:
        1. kui arve saaja on registreeritud käibemaksukohustuslane, siis rakendame 0% km-määra,
        1. kui arve saaja ei ole registreeritud käibemaksukohustuslane, siis on tegemist kaugmüügiga ja:
            1. kui StarCloud ei ole registreeritud OSS korra kasutaja, siis rakendame Eesti kohalikku km määra (9%)
            1. kui StarCloud on registreeritud OSS korra kasutaja, siis rakendame tellija asukohamaa km määra. NB! OSS kohustusliku registreerimise piir on 10 tuhat eurot käivet mittekäibemaksukohustuslastele üle kogu EL (va Eesti). OSS kora kasutamise otsustame jooksvalt iga aasta ja see algab fikseeritud kuupäevast.
    1. Kui arve saaja on üldse väljaspool Euroopa Liitu, siis on igal juhul 0% km-määr.
1. Kauba saaja andmed Opiq-AccountStudio vahelises andmevahetuses, tärniga on tähistatud kohustuslikud väljad:
    1. Unikaalne võti/kood Opiqus*
    1. Nimi* - ettevõtte nimi, füüsilise isiku puhul ees- ja perekonnanimi
    1. Riik* - P1
        1. Koolide puhul teada.
        1. **Kuid puudub hetkel Opiqus eraklientide puhul, hakkame nüüd koguma.**
            1. Ideaalis tuleks seda küsida liitumisel
            1. Kuid et palju erakliente on juba olemas, siis peame seda küsima tellimisel neile, kelle puhul me seda ei tea.
    1. Aadress* - puudub nii koolide, kui eraklientide puhul, hakkame nüüd koguma. P1
        1. Koolide puhul korjame Heikilt või kuskilt HM'ist, hiljem teeme sisestusväljad.
        1. Erakasutajate puhul küsime tellimisel.
        1. Koosneb järgmistest väljadest:
            1. Maakond
            1. Linn või asula
            1. Tänav või asula
            1. Postiindeks
    1. Ettevõtteregistri number - juriidiliste isikute puhul. Puudub hetkel Opiqus, tuleks võimaldada sisestada, P1, korjame koolide puhul Heikilt.
    1. Mudeli täiendused kauba saaja, ehk kliendikonto osas Opiqus:
        1. Ettevõtteregistri number
        1. Aadress - vähemalt koolile
    1. Erakasutaja tellimise täiendused Opiqus:
        1. Riigi ja aadressi sisestamise võimalus
    1. Kooli lisamise täiendused Opiqus:
        1. Ettevõtteregistri numbri lisamise võimalus
        1. Aadresi lisamise võimalus
1. Maksude arvutamine arvel:
    1. Opiq teab täishinda ja käibemaksumäära - **viimane leitakse arve saaja järgi**.
    1. Arvutame käibe ja ümardame selle kahe kohani pärast koma - käive = täishind / (1 + käibemaksumäär). Näiteks kui täishind on 7 ja käibemaksumäär on 0,09, siis käive on 6,42.
    1. Arvutame käibemaksu - käibemaks = täishind - käive.
1. Arve saaja andmed Opiq-AccountStudio vahelises andmevahetuses:
    1. Nimi* - ettevõtte nimi, füüsilise isiku puhul ees- ja perekonnanimi
    1. Riik* - koolide puhul teada, kuid puudub hetkel Opiqus füüsilise isiku puhul, tuleb küsima hakata, P1.
    1. Aadress* - koolide puhul teada, kuid puudub hetkel Opiqus füüsilise isiku puhul, tuleb küsima hakata, P1.
    1. Ettevõtteregistri number* - juriidiliste isikute puhul, selle järgi saadame e-arve, AccountStudio saadab.
    1. Käibemaksukohustuslasena registreerimise (KMKR) number - hetkel puudub Opiqus, teeme võimalikuks selle sisestamise, P1.
    1. E-postiaadress:
        1. Vajame, et kellegiga suhelda, kui ilmnevad probleemid.
        1. Kui arvet ei saadeta e-arvete süsteemi kaudu, siis saadame arve sellele e-postiaadressile, Opiq saadab.
    1. Mudeli täiendused arve saaja osas Opiqus:
        1. KMKR number
    1. Tellimise täiendused Opiqus:
        1. Eraisikust arve saajal peame hakkam küsima riiki ja aadressi
        1. KMKR number
1. Küsimused:
    1. Riiki küsime sõltumata sellest, kas tegu on juriidilise või füüsilise isikuga? Jah.
    1. Mis me füüsilise isikuga teeme? Ei ole vahet, kas füüsiline isik (füüsilisest isikust ettevõtja) saab olla käibemaksukohustuslane.
    1. Kust kontrollida EL käibemaksukohustuslaseks olijat? Väite õigsust saame ja peame kontrollida.
    1. Sõltumata sellest, kas KM on 0, 9 või x - võime ikka küsida seda hinda, mida me küsime? Jah.
    1. E-arvete küsimused:
        1. Kas e-arveid saadame ostja või maksja järgi? Maksja järgi saadame.
        1. Kas me peaks koolidel laskma valida arve saamise viisi - e-kiri või e-arve? Ei.
    1. Kummal poolel arve koostatakse, Opiq või AccountStudio? Opiq.
1. Käsitsi saab käibemaksukohustuslase andmed ettevõtteregistri numbri järgi teada siit [https://maasikas.emta.ee/saqu/public/kmkrnr](https://maasikas.emta.ee/saqu/public/kmkrnr), raha ei küsita.
1. Masin-masina jaoks on X-tee päringud, [https://www.emta.ee/ariklient/e-teenused-koolitused/e-teenuste-kasutamine/x-tee-teenused](https://www.emta.ee/ariklient/e-teenused-koolitused/e-teenuste-kasutamine/x-tee-teenused).
1. X-tee eeldab vist ka päringu sooritaja isikukoodi või lausa sisselogitust?
1. X-tee eeldab turvaserverit, seda teenust saab osta, [https://abi.ria.ee/xtee/et/x-tee-juhend/kuidas-kasutada-x-teed/x-tee-kasutamisvoimaluste-ja-kulude-uelevaade/majutatud-turvaserver](https://abi.ria.ee/xtee/et/x-tee-juhend/kuidas-kasutada-x-teed/x-tee-kasutamisvoimaluste-ja-kulude-uelevaade/majutatud-turvaserver)
1. Auto-complete'i tarbeks mõeldud tasuta päring [https://www.rik.ee/sites/www.rik.ee/files/elfinder/article_files/autocomplete_est.pdf](https://www.rik.ee/sites/www.rik.ee/files/elfinder/article_files/autocomplete_est.pdf).

## Arve raamatupidamisse saatmise endpoint

Näide:

```
{
	"header":
		{
			"invoiceNo":"EOPIK000002",
			"customerId":"10400025",
			"registerCode":"20161212",
			"customerName":"Tartu Reaalkool",
			"customerType":"School",
			"addressLine1":"Kalja 33-4",
			"addressLine2":"Tartu linn",
			"addressLine3":"Tartumaa",
			"zipCode":"67812",
			"countryCode":"EE",
			"country":"Eesti",
			"docDate":"2020-12-12",
			"dueDate":"2020-12-21",
			"paymentAmount":"201.60",
			"paymentAmountWithoutVat":"184.95",
			"vatAmount":"16.65",
			"vatRateCode":"9-ee",
			"vatRate":"0.09",
			"paymentMethod":"BankLink"
		},
	"recipient":
		{
			"recipientType":"Organization",
			"recipientName":"Tartu Linnavalitsus",
			"emailaddress":"taru@tartu.ee"
			"registerCode":"20161212",
			"vatRegNo":"EE101902269",
			"addressLine1":"Kalja 33-4",
			"addressLine2":"Tartu linn",
			"addressLine3":"Tartumaa",
			"zipCode":"67812",
			"countryCode":"EE",
			"country":"Eesti"
		},
	"lines":
		[
			{
				"packageID":"0100010102",
				"packageName":"Õpilase pakett",
				"periodStart":"2020-12-22",
				"periodEnd":"2021-02-25",
				"quantity":"21",
				"paymentAmount":"191.10",
				"paymentAmountWithoutVat":"175.32",
				"vatAmount":"15.78",
				"vatRateCode":"9-ee",
				"vatRate":"0.09"
			},
			{
				"packageID":"0100010103",
				"packageName":"Õpetaja pakett",
				"periodStart":"2016-12-22",
				"periodEnd":"2017-03-15",
				"quantity":"3",
				"paymentAmount":"10.50",
				"paymentAmountWithoutVat":"9.63",
				"vatAmount":"0.87",
				"vatRateCode":"9-ee",
				"vatRate":"0.09"
			}
		]
}
```

1. Vaata ka [https://www.avita.ee/astra/apidoc#post-invoice](https://www.avita.ee/astra/apidoc#post-invoice).
1. Post päring url-parameetriga `environment`, milles olev tähistab Opiqu keskkonda, millest päring sooritatakse:
    1. "opiq.ee" - https://www.opiq.ee/, "päris" Eesti Opiq
    1. "opiq.fi" - https://www.opiq.ee/, "päris" Soome Opiq
    1. "astra-test.ee" - https://astra-test.azurewebsites.net/, Opiqu peamine testkeskkond, siiani on Heikil test- ja laivkeskkondade jaoks lisaks eraldi endpoindid, et turvameetmeid oleks topelt konfiguratsioonivigade vastu
    1. ...
1. `header.customerId` - kauba saaja, Opiqu mõistes kliendi võti
1. `header.registerCode` - kauba saaja Ettevõtteregistri number, eraisikust kliendi puhul jääb tühjaks
1. `header.customerName` - kauba saaja nimi, ettevõtte/kooli nimi või eraisikust kliendi puhul ees- ja perekonnanimi
1. `header.customerType` - kauba saaja (st kliendi) tüüp: "School" või "PrivateCustomer" - erakasutaja, peaks olema eraisik
1. `header.countryCode` - kauba saaja riigi ISO 3166-1 alpha-2 kood
1. `header.docDate` - arve koostamise kuupäev
1. `header.dueDate` - arve tähtaeg
1. `header.paymentMethod` - maksmise meetod
    1. "BankLink" - tasuti Maksekeskuse/pangalingi kaudu
    1. "Invoice" - tasutakse pangaülekandega arve alusel
    1. "CreditCard" - tasuti Maksekeskuse/krediitkaardi abil
1. `recipient` - arve saaja andmed
    1. `recipientType` - "Organization"/"PrivatePerson"
    1. `vatRegNo` - KMKR number
    1. `countryCode` - arve saaja riigi ISO 3166-1 alpha-2 kood
1. `lines` - arve read, üks rida = ühte tüüpi litsentside tellimus
    1. `packageID` - litsentsi paketi kood, arve väljatrükil "Toode"
    1. `packageName` - paketi nimi´, arve väljatrükil "Kirjeldus"
    1. `periodStart` - litsentside kehtivusperioodi alguse kuupäev, arve väljatrükil "Periood"
    1. `periodEnd` - litsentside kehtivusperioodi lõpu kuupäev, arve väljatrükil "Periood"
    1. `quantity` - litsentside arv', arve väljatrükil "Kogus"

## Maksmata arvete päring

1. Vaata ka [https://www.avita.ee/astra/apidoc#get-unpaid](https://www.avita.ee/astra/apidoc#get-unpaid).
1. Get päring url-parameetriga `environment`, muu sisend puudub, peab tagastama sisendiga määratud keskkonna arvete numbrid, mis on täielikult maksmata.

Päringu vastus:
```
{
  "invoiceNumbers": [
    "A-17-000331",
    "A-17-000431"
  ]
}
```

## Arve makstuse päring

1. Vaata ka [https://www.avita.ee/astra/apidoc#billing-info](https://www.avita.ee/astra/apidoc#billing-info).
1. Get päring url-parameetriga `environment`.

Sisend (teeme siis senisest teisiti):
```
{
  "invoiceNumber": "A-17-000331"
}
```

Väljund:
```
{
    "invoiceNumber": "A-17-000331",
    "wasFound": true,
    "invoiceDetails": {
        "paymentAmount": "11.50",
        "paidAmount": "10.50"
    }
}
```

1. `wasFound` - `false`, kui sellise numbriga arve puudub.