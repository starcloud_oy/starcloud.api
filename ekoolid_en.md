### API for grades

Some conventions:

1. If input or output parameter is named `id` then it contains object (journal, task) identifier issued by Opiq
1. If input or output parameter is named `external_id` then it contains object identifier issued by eKool. 
1. The object identifiers used in the service address are eKool identifiers.

#### Requesting grades from Opiq

GET `https://.../api/journals/{journal id in eKool}/assignments/{task id in eKool}/grades?ui_language={language code en|et|ru|fi}`. [Basic authentication](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Authorization), username and password are the same Client ID and Client secret used by Opiq requesting token from eKool authentication server.

Optional query parameter `ui_language` allows to pass UI language, error messages are returned in this language, supported values are `et`, `en`, `ru` ja `fi`, default is 'et'.

In case of success:

```
{
    "grades": [
        {
            "student": {
                "id": "id in eKool"
            },
            "is_assessed": true, // Is it (result) assessed and disclosed to the student?
            "assessment": {
                "assessment_method": "grade",
                "score": 99,
                "grade": "A++",
                "teacher_comment": "Good job!"
            },
            "last_changed": "2019-04-24T14:30:21" // ISO 8601, Estonian time zone
        },
        {
            "student": {
                "id": "id in eKool"
            },
            "is_assessed": true,
            "assessment": {
                "assessment_method": "score",
                "score": 55
            },
            "last_changed": "2019-04-24T13:39:56"
        },
        {
            "student": {
                "id": "id in eKool"
            },
            "is_assessed": false,
            "last_changed": "2019-04-24T13:59:31"
        }
    ]
}
```

All the students who were given the assignment are listed.

Object `assessment` is existing is response, if result of the task is disclosed to the student.

Possible values of `assessment_method`:

- "grade" - "ordinary" grade, 1-5, A, B, ... . In this case `grade` is set.
- "score" - in this case the teacher did not grade the student, `grade` is missing and only `score` is set (`teacher_comment` may be set).
- "formativelyAssessed" - teacher assessment is in field `teacher_comment`, `score` may be set.

`score` may be set always, in case of any `assessment_method` value.

`score` may be missing, for exmaple it is possible to mark all the exercise units as "unvalued", but it is a rare case. In case of missing score the `assessment_method` cannot be "score", but "grade" and "formativelyAssessed" are valid values.

`grade` is set only when `assessment_method` is "grade".

Teacher may always enter something in field `teacher_comment`, in case of any `assessment_method` value. If `assessment_method` is "formativelyAssessed", then `teacher_comment` contains this assessment.

`last_changed` is always set.

The teacher may withdraw the assessment, i.e. if the query is made at the moment the assessment is disclosed to the student, the field `is_assessed` is "true" and the field `assessment` is set. If the query is then made at the moment when the teacher has withdrawn the assessment, the field `is_assessed` is "false" and the field `assessment` is unspecified, `last_changed` indicates the date and time of this change.

If task with the given `external_id` is missing in Opiq, then HTTP 404 (Not Found) and following JSON is returned:

```
{
    "status": "error",
    "error_message": "Task not found."
}
```

In case of any other error HTTP 400 (Bad request) and following JSON is returned:

```
{
    "status": "error",
    "error_message": "Error message."
}
```

#### Requesting assigments list from Opiq

GET `https://.../api/journals/{journal id in eKool}/assignments?ui_language=et`

In case of success:

```
{
    "assignments": [
        {
            "id": "assignment id in Opiq",
            "external_id": "assignment id in eKool",
            "is_assigned_to_all": false, // true, if task was assigned to all the students
            "targets": ["user id in eKool", "user id in eKool"] // the students to whom the task was assigned, presented when is_assigned_to_all is false
            "assigned_by": "teacher id in eKool", // id of the teacher by whom the task was assigned
            "type": "homework", // "homework" or "assignment"
            "title": "Volcanoes", // max 255 symbols
            "description": "Solve exercises", // max 4000 symbols
            "url": "https://www.opiq.ee/task/ekool/12345", // task url in Opiq
            "deadline": "2017-05-31" // YYYY-MM-DD
        },
        {
            "id": "57942351325",
            "external_id": "45212",
            "is_assigned_to_all": true,
            "assigned_by": "5791134",
            "type": "assignment",
            "title": "Work in groups",
            "description": "Jigsaw 10 000",
            "url": "https://www.opiq.ee/task/ekool/45212",
            "deadline": "2017-05-15"
        }
    ]
}
```

All tasks of the journal will be returned, regardless of whether they have been transferred to eKool or not.

If a task is not transferred to eKool, the `external_id` field will be missing in the message part of the task.

If journal with the given `external_id` is missing in Opiq, then HTTP 404 (Not Found) and following JSON is returned:

```
{
    "status": "error",
    "error_message": "Journal not found."
}
```

In case of any other error HTTP 400 (Bad request) and following JSON is returned:

```
{
    "status": "error",
    "error_message": "Error message."
}
```
