# eKooli-Opiq liides

1. eKool ja Star Cloud peavad sünkima koolid. See on vajalik, et eKool ja Star Cloud kliendiinfo alusel koole topelt ei tekiks. eKooliga peaks koolide andmed saama sünki läbi koolide EHISe koodi: nii pole mingit spetsiaalset andmete ülevaatust ja korrastust vaja, andmeid vahetades kasutame koolide EHISe koode (näit Rocca al Mare Kooli kood on 741).
1. UC1 - kooli administraator peab Opqusse looma kooli konto (ja koos sellega iseenda kasutajakonto).  
See on eelduseks, et saaks õpilastele kasutuslitsentse tellida ja toimub Star Cloud tellimiskeskkonna kliendiinfo alusel (lehel [https://www.opik.ee/Catalog](https://www.opik.ee/Catalog), Opiqu kasutajatele > Kool > "Tee kooli esindaja konto!"). Selleks, et õpetajad saaksid päevikuid Opiqus registreerida kooli administraatori konto vajalik pole, küll peab aga kool olema Opiqusse loodud.
1. UC2 - kooli administraator tellib Opiqus õpilastele litsentsid. Vajalik, et õpilased saaks Opiqus teoseid kasutada, päeviku registreerimiseks otseselt vajalik pole.
1. UC3 - õpetaja registreerib Opiqus päeviku
    1. eKool teeb oma rakendusse nupu, mille vajutamisel saadetakse kasutaja oma vooga Opiqusse lehel, kus kasutaja valib teose ja kinnitab päeviku registreerimise.
    1. Opiqus:
        1. valitakse teos, mida saavad selle päeviku õpetajad ja õpilased kasutama
        1. luuakse õpetaja enda kasutajakonto
        1. teiste selle päeviku õpetajate kasutajakontod
        1. õpilaste kasutajakontod
        1. päevik koos kõigi vajalike seostega - õpetaja/õpilased-päevik, päevik-teos, õpetaja/õpilased-teos
1. UC4 - Päeviku andmete uuendamine. Öösel uuendatakse Opiqus kõik eksporditud päevikuid.
1. UC5 - eKoolist sso abil sisenemine Opiqusse koos rolli valikuga - admin, õpetaja või õpilane - siis saavad kasutajad eelnevate kasutuslugudega loodud kraami lõpuks kasutada.
1. UC6 - nn permalink - link, mille õpetaja saab lihtsalt kopeerida eKooli päevikusse kodutöösse/ülesandesse ja mida klikkides ning Opiqusse sisselogides satub õpilane seda tööd lahendama või tulemust vaatama ja õpetaja seda kontrollima.
1. UC7 - Opiqust kodutöö/ülesande saatmine eKooli.
1. UC8 - Opiqust hinde saatmine eKooli.

### Autentimine eKoolis

eKooli klientidel ja partneritel on võimalik enda süsteemi kasutajate tuvastamiseks rakendada eKooli autentimislahendust. Sarnaselt pangalingi, mobiil-ID, ID kaardi, Google või Facebooki kontoga sisselogimisele on võimalik oma süsteemidesse sisse logida ka eKooli kasutajatunnustega.

Autentimislahendus on üles ehitatud kasutatud avatud standardit - Open Auth 2.0 ([http://oauth.net/2/](http://oauth.net/2/)).

Autentimislahenduse kasutamiseks tuleb kliendi rakendus registreerida eKoolis. Selleks on vajalik teada anda:

- Millises ulatuses soovib kliendi rakendus autenditud kasutaja andmetele ligi pääseda/milliseid päringuid kasutaja nimel teha. Minimaalselt antakse ligipääs kasutaja põhiandmetele (nimi, isikukood, isikukoodi väljaandja riik).
- Aadress, kuhu kasutaja peale autentimist suunatakse.

Vastavalt kokkuleppele annab eKool kliendi süsteemile parameetrid:

- Client ID
- Client secret
- Scope

Autentimisprotsessi kirjeldus:

- Kliendi rakendus suunab kasutaja eKooli autentimislehele, https://auth.ekool.eu/auth/oauth/authorize
- Kasutaja sisestab enda kasutajanime+parooli ja lubab Kliendi rakendusele ligipääsu oma andmetele (vastavalt soovitud ulatusele).
- eKooli autentimislahendus suunab kasutaja tagasi Kliendi rakendusse andes kaasa vajaliku access_token'i küsimiseks.
- Kliendi rakendus küsib aadressilt eKooli autentimislahenduselt access_token'i,  https://auth.ekool.eu/auth/oauth/token
- Kliendi rakendus saab kasutaja nimel pöörduda eKooli süsteemi poole (enda secretit ja access_token'it kasutades) päringute tegemiseks vastavalt lubatud ulatusele, kasutaja Opiqus autentimiseks on vaja teada kasutaja põhiandmeid, selleks kasutatakse kasutaja andmete päringut eKoolist

### Kasutaja andmete päring eKoolist

Päringuga https://rest.ekool.eu/opiqdata saab küsida eKoolist autenditud kasutaja andmed, vastus:

```
{
    "name_first": "kasutaja eesnimi",
    "name_last": "kasutaja perekonnanimi",
    "userName": "kasutajatunnus",
    "id": "kasutaja id",
    "emails": ["kasutaja kinnitatud e-posti aadress 1", ...],
    "institutions": [{"id": "kooli EHIS'e kood, kus kasutaja on tegev 1", "label": "Kooli nimetus"},{…},...],
    "roles": [ // kasutaja rollid erinevates koolides
        {
            "client": {
                "id": "kooli EHIS'e kood, kus kasutaja on tegev 1",
                "label": "Kooli nimetus 1",
    		},
            "is_student": false, // kas kasutaja on selles koolis õpilane
			"is_teacher": true, // kas kasutaja on selles koolis õpetaja
            "is_parent": true // kas kasutaja on selles koolis lapsevanem
        },
        {
            "client": {
                "id": "kooli EHIS'e kood, kus kasutaja on tegev 2",
                "label": "Kooli nimetus 2",
    		},
            "is_student" false, // kas kasutaja on selles koolis õpilane
			"is_teacher": true, // kas kasutaja on selles koolis õpetaja
            "is_parent": false // kas kasutaja on selles koolis lapsevanem
        }
    ],
	"students": [ // kasutaja lapsed
    	{
            "userName": "kasutajatunnus",
        	"id": "kasutaja ID",
      		"emails": ["kasutaja kinnitatud e-posti aadress 1", ...],
            "name_first": "kasutaja-eesnimi",
            "name_last": "kasutaja-perenimi",
        	"client": { // lapse kool
            	"id": "kooli EHIS'e kood",
            	"label": "Kooli nimetus",
        	}
    	}
	]
}
```

Päringu vastuses tagastatakse  kasutajate kinnitatud meiliaadresse, neid võib olla 0 ja enam kui üks. Kasutaja kasutajatunnus võib olla meiliaadress või isikukood. Isikukoodidest vabanemisega tegeletakse,kuid peame arvatavasti arvestama nendega veel pika aja jooksul.

### Client ID ja Client secret

Authorization-päises kasutame /auth/oauth/authorize ja /auth/oauth/token päringu korral [Basic autentimisskeemi](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Authorization). Päringute tegemisel kasutatakse [Bearer autentimisskeemi](https://tools.ietf.org/html/rfc6750).

### Kasutaja väljalogimine eKoolis

Probleem - on võimalik, et kasutaja logib ennast eKooli sisse ainult enda autentimiseks Opiqus, siis võib eKooli sessioon ripakile jääda. Et sellest tingitud turvariski vähendada võime teha sellise lahenduse, et kui kasutaja logib ennast Opiqust välja ja ta on ennast autentinud eKooli abil, siis pakutakse talle võimalust ennast ka eKoolist välja logida.

Selle tarbeks võiks autentimise järgselt eKoolist sooritatud päringu vastus sisaldada kasutaja (sessiooni?) põhist url'i https://ekool.eu/index_et.html#/?screenId=g.user.logout , millele HTTPS GET päringut sooritades logitakse kasutaja eKoolist välja.

### Päeviku registreerimise API

#### Päeviku registreerimise aadress Opiqus

Kui õpetaja alustab eKoolis päeviku, mille võti on `12345` registreerimist Opiqus, siis suunab eKool kasutaja sirvijas Opiqu aadressile `https://www.opiq.ee/ImportRegister/EKool?registerId=12345` (HTTPS GET). Päringu tegemisel lisatakse päringu päise autoriseerimiskirjesse lühikest aega eKoolis kehtiva Bearer Token (Authorization: Bearer bearertoken), mille abil Opiq saab eKoolist teenusega `journal` küsida päeviku andmed. Päringus kasutatav parameeter`registerId` on registreeritava päeviku võti.

Päevikut ei saa registreerida Opiqus kui:

- kooli administraator ei ole eKooli keskkonnas ära määranud kooli ehise koodi
- kool on eKooli keskkonnas avalikustamata
- üritatakse registreerida eelmise aasta päevikut

Kui päevik on Opiqus juba registreeritud ja kasutaja alustab eKoolist uuesti päeviku registreerimist Opiqusse, siis kasutajale ei väljastata viga, vaid informeeritakse kasutajat päeviku registreeritusest Opiqus ning kasutaja soovi korral uuendatakse Opiqus päeviku andmeid.

#### Päeviku andmete päring eKoolis

Päeviku registreerimiseks sooritab Opiq eKooli päringu kasutades eelnevalt saadud päeviku võtit `https://rest.ekool.eu/journal/12345` (HTTPS POST), lisades selle Authorization päisesse eKoolist suunamisel edastatud Bearer Token'i.

eKool tagastab vastuse JSON formaadis. Vastuses ei tagastata `student` osas õpilasi, kellel ei ole eKoolis kehtivat kasutajakontot:

```
{
    "current_user": { // eKooli kasutaja, kes nuppu vajutas päeviku üle kandmiseks
  		"userName": "kasutajatunnus",
        "id": "kasutaja ID",
      	"emails": ["kasutaja kinnitatud e-posti aadress 1", ...],
        "name_first": "kasutaja-eesnimi",
        "name_last": "kasutaja-perenimi"
    },
    "client": {
        "id": "kooli-EHIS-kood",
        "label": "kooli-nimi",
    },
    "id": "päeviku-võti-eKoolis",
    "no_of_students": Õpilaste_arv_päevikus,
    "startDate": "Päeviku alguskuupäev",
    "endDate": "Päeviku lõpukuupäev",
    "label": "7a Geograafia",
    "teachers": [ // Päeviku õpetajad (üks kuni mitu)
        {
            "userName": "kasutajatunnus",
        	"id": "kasutaja ID",
      		"emails": ["kasutaja kinnitatud e-posti aadress 1", ...],
            "name_first": "kasutaja-eesnimi",
            "name_last": "kasutaja-perenimi"
        }
    ],
    "students": [
        {
            "userName": "kasutajatunnus",
        	"id": "kasutaja ID",
      		"emails": ["kasutaja kinnitatud e-posti aadress 1", ...],
            "name_first": "kasutaja-eesnimi",
            "name_last": "kasutaja-perenimi"
        },
        {
            "userName": "kasutajatunnus",
        	"id": "kasutaja ID",
      		"emails": ["kasutaja kinnitatud e-posti aadress 1", ...],
            "name_first": "kasutaja-eesnimi",
            "name_last": "kasutaja-perenimi"
        }
    ]
}
```

Opiq saab hiljem päeviku andmeid eKoolist pärida kasutades päringut `https://rest.ekool.eu/journal/<päeviku-võti-eKoolis>` autentides eelnevalt CLIENT_CREDENTIALS meetodiga autentimisserveris. Selliselt esitatud päringu vastuses puudub element `current_user`.

Kui sellist päevikut pole eKoolis (näiteks sellepärast, et see kustutati ja see trigerdaski päeviku uuendamise Opiqus), siis tagastatakse vastuseks HTTP 404  {"error": "Not Found"}.

#### Päeviku eKooli-Opiqu seose aktiveerimine eKoolis

Pärast päeviku edukat registreerimist Opiqus peaks/võiks Opiq aktiveerida seose eKoolis, st saata eKooli info selle kohta, et päevik on Opiqus registreeritud ja seda kahel põhjusel:

1. Nii teaks eKool, milliste päevikut muutumisel Opiqut sellest teavitada.
1. Opiq saaks selle väljakutsega edastada eKooli nn õppematerjali aadressi, st Opiqu aadressi, mida saab eKoolis kuvada ja millele minnes eKooli kasutajale avaneks päevikuga Opiqus seotud teos (seda funktsionaalsust hetkel ei kasutata).

##### Aktiveerimine

Opiq teeb päringu aadressile `https://rest.ekool.eu/journallink/<päeviku-võti-eKoolis>` (HTTPS POST) autentides ennast eelnevalt CLIENT_CREDENTIALS meetodiga ning lisades autentimisserveri poolt tagastatud access_tokeni Authorization Bearer päisesse.

Päringuga pannakse kaasa järgmine sõnum JSON formaadis:

```
{
    "action": "activate",
    "thirdparty_label": "Teose inimkeelne nimi, näiteks '20. sajandi kirjandus gümnaasiumile'",
    "thirdparty_url": "Teose aadress Opiqus https://..."
}
```

eKooli vastus JSON formaadis:

```
{
    "status": "ok"
}
```

Kui vaja, siis võib Opiq anda seosele omalt poolt unikaalse võtme, Opiq edastaks selleks päeviku võtme enda süsteemis.

##### Muutmine - see osa ei ole realiseeritud, linkide muutmist praegu ei toimu!

Vajalik näiteks juhuks, kui muutub päeviku teos.

Opiq teeb päringu aadressile `https://<eKooli keskkonna juuraadress>/.../journal-link/<päeviku-võti-eKoolis>` (HTTPS POST), lisades selle Authorization päisesse eelnevalt eKooliga kokku lepitud CLIENT_ID ja CLIENT_SECRET parameetrid.

```
{
    "action": "update",
    "thirdparty_label": "Teose inimkeelne nimi, näiteks '20. sajandi kirjandus gümnaasiumile'",
    "thirdparty_url": "Teose aadress Opiqus https://..."
}
```

eKooli vastus JSON formaadis:

```
{
    "status": "ok"
}
```

##### Kustutamine

Opiq kasutaks seda järgmistel juhtudel:

1. Kasutaja saab Opiqus päeviku kustutada (eKoolis päeviku kustutamist me esile kutsuda ei soovi) - siis saadaksime seose kustutamise sõnumi.
1. Uut õppeaastat alustades päevikud arhiveeritakse - ka siis saadaksime päevikute kohta seose kustutamise sõnumid.

Opiq teeb päringu aadressile `https://rest.ekool.eu/journallink/<päeviku-võti-eKoolis>` (HTTPS POST) autentides ennast eelnevalt CLIENT_CREDENTIALS meetodiga ning lisades autentimisserveri poolt tagastatud access_tokeni Authorization Bearer päisesse.

```
{
    "action": "delete"
}
```

eKooli vastus JSON formaadis:

```
{
    "status": "ok"
}
```

#### Opiqu teavitamine muutunud päevikutest – see osa ei ole realiseeritud, Opiqu teavitamist muutunud päevikutest ei toimu!

eKool teavitab aktiveeritud seoste järgi Opiqut nende päevikute muudatustest. Teavitatakse õpetajate lisamisest/eemaldamisest, õpilaste lisamisest/eemaldamisest, päeviku nime muutusest ja õpetajate ning õpilaste nime muutustest. Ei teavitata päeviku lõpetamisest seoses õppeaasta lõppemisega.

Selleks peab eKool sooritama päringu aadressile `https://<Opiqu keskkonna juuraadress>/Ekool/JournalUpdates` (HTTPS POST), lisades selle Authorization päisesse eelnevalt eKooliga kokku lepitud CLIENT_ID ja CLIENT_SECRET parameetrid.

Edastada tuleb JSON sõnum:

```
{
    "action": "journal-updates",
    "data": {
        "journals": ["päeviku-võti-eKoolis-1", "päeviku-võti-eKoolis-2"]
    }
}
```

Opiq tagastab sõnumi edukal registreerimisel vastuse JSON formaadis:

```
{
    "status": "ok"
}
```

Päevikud töödeldakse asünkroonselt, pärast sõnumi registreerimist ja vastuse tagastamist eKoolile.

### Töö saatmine

Töö osadeks Opiqus on töö pealkiri, tähtaeg ja üks või enam ülesannet, ülesannetel on samuti pealkirjad.

Töö lisamisel teeb Opiq eKooli aadressile `https://<eKooli keskkonna juuraadress>/.../journal-assignment/<päeviku-võti-eKoolis>` (HTTPS POST) päringu. Opiq autendib ennast eelnevalt eKooli autentimisserveris OAuth Client Credential meetodil, saadud tokenit kasutatakse `journal-assignment` päringu tegemisel.

Töö saatmiseks edastatakse JSON sõnum:

```
{
    "action": "create", // Lisa töö
    "ui_language": "et", // Opiqu kasutajaliidese keel
    "is_assigned_to_all": true, // true, kui töö lisati kõigile päeviku õpilastele
    "targets": ["kasutaja ID", "kasutaja ID"] // Õpilaste kasutajate ID-d, kellele töö lisati, täidetud ka siis, kui is_assigned_to_all on true
    "assigned_by": "kasutaja ID", // Õpetaja kasutaja ID, kes töö lisas
    "assignment": {
        "type": "homework", // Töö tüüp
        "title": "Töö pealkiri/teema, kuni 255 sümbolit pikk tekst",
        "description": "Töö sisu tekstina, kasutatakse reavahesid, pikkus kuni 4000 sümbolit.",
        "url": "https://www.opiq.ee/task/ekool/12345", // Aadress, kust avaneb töö nii õpilastele kui õpetajatele
        "deadline": "2017-05-31" // Töö tähtaeg YYYY-MM-DD formaadis, kontrolltöö puhul toimumise kuupäev
        "assignment_thirdparty_id": "fafefb00-0586-45ab-9a93-480a045e1490" // Töö võti Opiqus, string, kuni 255 sümbolit pikk
    }
}
```

`ui_language` võimalikud variandid on `et`, `en`, `ru` ja `fi`. Selles keeles võiks tagastada lõppkasutajale mõeldud veateated. Kui mingi keel pole eKoolis toetatud, siis tagastada veateated eesti keeles. 

Kui eKool ei toeta töö andmist ainult osadele päeviku õpilastele, siis võib parameetrit `targets` sisuliselt ignoreerida - Opiqus on tööle ligipääs lahendatud nii, et õpilane, kellele seda ei määratud, tööle ligi ei pääse ja talle kuvatakse teade, mis informeerib teda sellest, et tema ei pea seda tööd tegema.

Parameetri `type` võimalikud väärtused on:

- `homework` - kodutöö
- `test` - kontrolltöö
- `assignment` - mingit muud tüüpi töö, eKoolis hindeline ülesanne

`title` sisuks on töö pealkiri Opiqus.

`description` sisuks on õpetaja sisestatud kommentaar tööle ja eraldi ridadel ülesannete pealkirjd, näiteks:

```
Lahendage peast!
Ülesanne 129
Ülesanne 130
Ülesanne 131
```

Kui töö lisamine õnnestus, siis tagastada sõnum:

```
{
    "status": "ok",
    "id": "12345" // Loodud töö võti eKoolis
}
```

Teenus peaks olema idempotentne selles mõttes et kui sõnum sama sisuga saadetakse uuesti, siis võiks Opiqule tagastada töö lisamise õnnestumise sõnumi. Et tegu on sama tööga, on võimalik tuvastada parameetri `assignment_thirdparty_id` väärtuse järgi. Töö andmeid uuesti saadetud sõnumi järgi muutma ei pea.

Kui töö lisamine ebaõnnestus, siis tagastada HTTP veakood 400 (Bad Request) ja JSON sõnumi:

```
{
    "status": "error",
    "error_message": "Inimloetav veateade, mida näidata õpetajale."
}
```

#### Töö valideerimine

Soovime Opiqus vältida olukorda, kus töö Opiqus on, aga eKoolis mitte, samuti soovime vältida vastupidist olukorda. Selliste olukordade tekke vähendamiseks oleks hea teenus, millega Opiq saab enne töö saatmist küsida eKoolist, kas soovitud andmetega töö saatmine/salvestamine eKooli on võimalik, selle väljakutse käigus võiks kontrollida ettenähtavaid veaolukordi (päevik on suletud, tähtaeg on väljaspool õppeperioodi jms). Teenus ja sõnumi sisu oleks samasugune, nagu töö saatmisel, vaid parameeter `action` väärtuseks edastatakse siis väärtus `validate` ja `assignment_thirdparty_id` väärtus on tühi.

Kui töö lisamine õnnestuks, siis tagastada sõnum:

```
{
    "status": "ok"
}
```

Kui töö lisamine ei ole võimalik, siis tagastada HTTP veakood 400 (Bad Request) ja JSON sõnumi:

```
{
    "status": "error",
    "error_message": "Inimloetav veateade, mida näidata õpetajale."
}
```

#### Töö kustutamine

Töö kustutamine Opiqus on võimalik, töö tuleks kustutada ka eKoolis. Selleks saadab Opiq teenusele `journal-assignment` järgmise JSON sõnumi:

```
{
    "action": "delete", // Kustuta töö
    "id": "12345", // Töö võti eKoolis
    "deleted_by": "kasutaja ID", // Õpetaja kasutaja ID, kes töö kustutas
}
```

Kui töö kustutamine õnnestus, siis tagastada sõnum:

```
{
    "status": "ok"
}
```

Kui töö kustutamine ei õnnestunud tehnilisel põhjusel, siis tagastada HTTP veakood 400 (Bad Request) ja JSON sõnumi:

```
{
    "status": "error",
    "error_message": "Inimloetav veateade, mida näidata õpetajale."
}
```

Kui tööd ei kustutatud äriloogilisel põhjusel, näiteks sellepärast, et töö külge on pandud hindeid, siis tagastada Opiqule eduka kustutamise sõnum.

### Hinnete pärimine Opiqust

Hinnete ülekandmise Opiqust eKooli initsialiseerib õpetaja eKoolis. eKool pärib Opiqust hinnete andmed, transformeerib Opiqu andmed sobivale kujule ning laseb õpetajal lõpuks hindeks salvestada.

Lahenduse eelised:

1. eKoolis saab iga kool endale defineerida meelepärase hindamissüsteemi, Opiqus selleks tugi puudub. Lahenduse puhul, kus eKool transformeerib ja õpetaja langetab lõpliku otsuse, pole erinevate hindamissüsteemide defineerimise tuge ja nende defineerimist Opiqusse vaja.
2. Õpetaja initsialiseerib hinnete ülekandmise ja seega kontrollib protsessi, asjad ei juhtu eKoolis temale teadmata.

Lahenduse miinused:

1. Lisatöö õpetajale.
1. Kui peale klassi hinnete ülekandmist muutub ühe õpilase hinne, siis peab õpetaja hinnete ülekandmise protsessi uuesti läbi viima, samuti võib ta selle vajaduse kergesti unustada.

Ka hinnete päringute puhul kasutavad eKool ja Opiq omavahel suheldes eelkõige eKooli võtmeid, hinnete pärimiseks Opiqust edastatakse Opiqusse eKooli töö võti. See tähendab, et eKoolis puuduva töö hinnete päringuks peab eKool töö kõigepealt vajaduse korral endale looma ja siis tööde vahelise seose Opiqusse salvestama - selleks on Opiqus omaette teenus, vaata allapoole.

**Küll on aga Opiqust sooritatavate päringute puhul tavapärasega võrreldes teistpidi töö võtmete `id` ja `external_id` tähendused sõnumite sisus**, täpsemad põhimõtted:

1. Kasutame eKooli võtmeid kus vähegi võimalik.
1. Teenuste aadressis kasutatavad võtmed on **eKooli** võtmed.
2. Nii sisend- kui väljund-JSON'is tähistab:
	1. `id` **Opiqu** võtit
	2. `external_id` **eKooli** võtit. 

Päevikuga seotud tööde pärimiseks Opiqust on Opiqus omaette päring.

#### Hinnete päring Opiqust

Hinnete pärimiseks teeb eKool aadressile `https://<Opiqu keskkonna juuraadress, näiteks opiq.ee>/api/journals/{päeviku võti eKoolis}/assignments/{töö võti eKoolis}/grades?ui_language=et` HTTPS GET päringu. Päringu autoriseerimiseks kasutame [Basic autentimisskeemi](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Authorization), kasutajatunnus ja parool on samad Client ID ja Client secret väärtused, mida Opiq kasutab *token*'i küsimisel eKooli autoriseerimisserverilt.

Mittekohustuslik päringu parameeter `ui_language` võimaldab edastada kasutajaliidese keele, selles keeles tagastatakse veateated. Opiq toetab valikuid `et`, `en`, `ru` ja `fi`. Kui edastatakse keele kood, mida Opiq ei toeta või jäetakse parameeter kaasa panemata, siis tagastatakse veateated eesti keeles.

Kui päring õnnestub, siis tagastab Opiq järgmise JSON formaadis sõnumi:

```
{
    "grades": [
        {
            "student": {
                "id": "kasutaja ID eKoolis"
            },
            "is_assessed": true, // Kas õpilase töö on hinnatud ja avaldatud või mitte?
            "assessment": {
                "assessment_method": "grade",
                "score": 99,
                "grade": "A++",
                "teacher_comment": "Hea töö!"
            },
            "last_changed": "2019-04-24T14:30:21" // "T" eraldab ISO 8601 järgi kellaaega kuupäevast, kellaaeg on eesti ajatsooni järgi
        },
        {
            "student": {
                "id": "kasutaja ID eKoolis"
            },
            "is_assessed": true,
            "assessment": {
                "assessment_method": "score",
                "score": 55
            },
            "last_changed": "2019-04-24T13:39:56"
        },
        {
            "student": {
                "id": "kasutaja ID eKoolis"
            },
            "is_assessed": false,
            "last_changed": "2019-04-24T13:59:31"
        }
    ]
}
```

Väljas `grades` on alati kõikide õpilaste kohta (kellele töö anti) element, sõltumata sellest, kas selle õpilase töö on hinnatud või mitte.

Väli `assessment` on õpilase sõnumi osas, kui tema töö on hinnatud, st `is_assessed` = `true`.

Võimalikud `assessment_method` väärtused:

- `grade` - sellisel juhul pani õpetaja õpilasele töö hindeks kas  "tavapärase" numbrilise hinde ühest-viieni või mingi muu hinde (mida ta nimetab hindeks), vastuses on siis täidetud väli `grade`
- `score` - sellisel juhul ei pannud õpetaja õpilasele hinnet ja ei hinnanud tööd ka kujundavalt, vaid avaldas õpilasele ainult töö skoori, väli `grade` puudub ja asjassepuutuvat on vaja otsida väljast `score`
- `formativelyAssessed` - õpetaja hindas töö kujundavalt, hinnangu leiab väljast `teacher_comment`

`score` võib olla määratud mistahes `assessment_method` väärtuse puhul - kui Opiqus on töö skoor arvutatud, siis see tagastatakse.

Väli `score` võib puududa - seda siis, kui skoori polnud võimalik arvutada, näiteks juhul, kui õpetaja jättis kõikide ülesannete vastused õpilase töös hindamata. Sellisel juhul ei saa `assessment_method` väärtus olla `score`, muud variandid on võimalikud (õpetaja saab panna hinde või hinnata õpilast kujundavalt, puuduvat skoori ta aga avaldada ei saa).

`grade` on täidetud ainult juhul kui `assessment_method` = `grade`.

`teacher_comment` võib olla täidetud mistahes hindamismeetodi puhul. Kui `assessment_method` = `formativelyAssessed`, siis sisaldub selles väljas õpetaja kujundav hinnang.

`last_changed` on alati määratud.

Õpetaja võib töö õpilasele avaldamise tagasi võtta, st kui päring teha hetkel, mil töö on õpilasele avaldatud, siis on väli `is_assessed` = `true` ja väli `assessment` määratud. Kui päring teha seejärel hetkel, mil õpetaja on avaldamise tagasi võtnud, siis on väli `is_assessed` = `false` ja väli `assessment` määramata, `last_changed` näitab selle muudatuse kuupäeva ja kellaaega.

Kui sellise võtmega tööd ei leita, siis tagastab Opiq HTTP veakoodi 404 (Not Found) ja JSON sõnumi:

```
{
    "status": "error",
    "error_message": "Tööd ei leitud."
}
```

Kui päringu sooritamisel juhtub muu viga, siis tagastab Opiq HTTP veakoodi 400 (Bad request) ja JSON sõnumi:

```
{
    "status": "error",
    "error_message": "Vea kirjeldus."
}
```

#### Tööde päring Opiqust

Päeviku tööde loetelu pärimiseks teeb eKool aadressile `https://<Opiqu keskkonna juuraadress, näiteks opiq.ee>/api/journals/{päeviku võti eKoolis}/assignments?ui_language=et` HTTPS GET päringu. Päring autoriseeritakse sama moodi, nagu hinnete päring Opiqust.

Mittekohustuslik päringu parameeter `ui_language` mõjub ja on kasutatav sama moodi nagu hinnete päringus.

Kui päring õnnestub, siis tagastab Opiq järgmise JSON formaadis sõnumi:

```
{
    "assignments": [
        {
            "id": "töö võti Opiqus",
            "external_id": "töö võti eKoolis",
            "is_assigned_to_all": false, // true, kui töö lisati kõigile päeviku õpilastele
            "targets": ["kasutaja ID", "kasutaja ID"] // Õpilaste kasutajate ID-d, kellele töö lisati, määratud siis, kui is_assigned_to_all on false
            "assigned_by": "kasutaja ID", // Õpetaja kasutaja ID, kes töö lisas
            "type": "homework", // Töö tüüp
            "title": "Töö pealkiri/teema, kuni 255 sümbolit pikk tekst",
            "description": "Töö sisu tekstina, kasutatakse reavahesid, pikkus kuni 4000 sümbolit.",
            "url": "https://www.opiq.ee/task/ekool/12345", // Aadress, kust avaneb töö nii õpilastele kui õpetajatele
        	"deadline": "2017-05-31" // Töö tähtaeg YYYY-MM-DD formaadis, kontrolltöö puhul toimumise kuupäev
        },
        {
            "id": "57942351325",
            "external_id": "45212",
            "is_assigned_to_all": true,
            "assigned_by": "5791134",
            "type": "assignment",
            "title": "Grupitöö",
            "description": "Tehke ristsõna",
            "url": "https://www.opiq.ee/task/ekool/45212",
            "deadline": "2017-05-15"
        }
    ]
}
```

Tagastatakse päeviku kõik tööd, sõltumata sellest, kas need on eKooli ülekantud või mitte.

Kui töö ei ole eKooli kantud, siis väli `external_id` puudub töö kohta käivas sõnumi osas.

Kui sellise võtmega päevikut ei leita, siis tagastab Opiq HTTP veakoodi 404 (Not Found) ja JSON sõnumi:

```
{
    "status": "error",
    "error_message": "Päevikut ei leitud."
}
```

Kui päringu sooritamisel juhtub muu viga, siis tagastab Opiq HTTP veakoodi 400 (Bad request) ja JSON sõnumi:

```
{
    "status": "error",
    "error_message": "Vea kirjeldus."
}
```

#### Töö eKooli võtme registreerimine Opiqus

Juhul kui õpetaja soovib pärida hinded töö kohta, mis eKoolis puudub, siis peab ta selle töö kõigepealt eKooli looma, seejärel registreerima töö eKooli võtme Opiqus soovitud töö juures.

Võtme registreerimiseks tuleb teha HTTP PUT väljakutse aadressile `https://<Opiqu keskkonna juuraadress, näiteks opiq.ee>/api/journals/{päeviku võti eKoolis}/assignments/link?ui_language=et`. Päring autoriseeritakse sama moodi nagu hinnete päring Opiqust.

Mittekohustuslik päringu parameeter `ui_language` mõjub ja on kasutatav sama moodi nagu hinnete päringus.

Sisendiks tuleb edastada järgmine JSON sõnum:

```
{
    "id": "töö võti Opiqus",
    "external_id": "töö võti eKoolis"
}
```

Kui sellise võtmega tööd ei leita, siis tagastab Opiq HTTP veakoodi 404 (Not Found) ja JSON sõnumi:

```
{
    "status": "error",
    "error_message": "Tööd ei leitud."
}
```

Kui päringu sooritamisel juhtub muu viga, siis tagastab Opiq HTTP veakoodi 400 (Bad request) ja JSON sõnumi:

```
{
    "status": "error",
    "error_message": "Vea kirjeldus."
}
```

Viga väljastatakse näiteks siis, kui:

- võtmega määratud töö ei ole päringu aadressiga määratud päeviku töö
- Opiqu võtmega määratud tööl on (teistsugune) eKooli töö võti määratud, viga ei väljastata, kui Opiqu tööle üritatakse külge panna juba küljesolevat eKooli töö võtitui töö ei ole eKooli kantud, s
- Opiqu võtmega määratud tööl ei ole veel eKooli võtit määratud, kuid töö on eKooli saatmisel - see on praktiliselt võimalik, töö saadetakse Opiqus koostamise järgselt eKooli asünkroonselt, töö saatmine võib viibida
- sõnumis edastatud eKooli võti on juba mingi Opiqu töö küljes
- jms