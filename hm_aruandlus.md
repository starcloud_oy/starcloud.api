# Aruandluse kirjeldus

## ChapterViewsStudent

Õpilaste peatükkide vaatamised. Ühes reas on peatükkide vaatamised nädala, peatüki, kooli ja õpilase klassi lõikes.

Veergude kirjeldused:

1. `IsoWeek` - ISO nädala (nädal algab esmaspäevaga) järjekorranumber aastas
2. `Kit` - teose pealkiri
3. `Chapter` - peatüki teema järjekorranumber, peatüki järjekorranumber teemas ja peatüki pealkiri
4. `PublishingHouse` - kirjastuse nimi
5. `School` - kooli nimi
6. `ClassNo` - õpilase klass, mis arvutatakse heuristiliselt tema uusima päeviku nime järgi, `0` - klass on teadmata
7. `ChapterViews` - reaga määratud peatükkide vaatamiste arv selles nädalas, koolis ja klassis
8. `AverageTimeOnPage` - reaga määratud peatükkide keskmine vaatamise aeg sekundites, keskmine on arvutatud selliste vaatamiste pealt, millel lehel viibimise aeg oli määratud
9. `KitId` - teose võti
10. `ChapterId` - peatüki võti
11. `SchoolCode` - kooli EHISe kood, `0` - tegu on välisriigis tegutseva kooliga, koolil puudub EHISes kood

Peatüki sisu vaatamiseks mine (õpetajana) aadressile https://www.opiq.ee/kit/{kitId}/chapter/{ChapterId}, näiteks https://www.opiq.ee/kit/71/chapter/5571 .

## TaskSolvingsStudent

Õpilaste ülesannete lahendamised. Ühes reas on õpetaja kodu- või tunnitööks antud ülesande lahendamise lehe avamised nädala, peatüki-ülesande, kooli ja õpilase klassi lõikes.

Veergude kirjeldused:

1. ...
2. `Exercise` - ülesande pealkiri
3. ...
4. `CountOfTaskSolvings` - reaga määratud ülesannete lahendamise lehtede arv selles nädalas, koolis ja klassis
8. `AverageTimeOnPage` - reaga määratud ülesannete lahendamise lehtede keskmine vaatamise aeg sekundites
9. ...
10. `ExerciseKey` - ülesande võti
11. ...

Ülesande sisu vaatamiseks mine (õpetajana) aadressile https://www.opiq.ee/kit/{KitId}/chapter/{ChapterId}/exercise?key={ExerciseKey} , näiteks https://www.opiq.ee/kit/6/chapter/211/exercise?key=7670dcc7-2523-473d-b413-f740b4f985d7 .

## ChapterViewsTeacher

Õpetajate peatükkide vaatamised. Ühes reas on peatükkide vaatamised nädala, peatüki, kooli ja õpetaja profiili lõikes.

Õpetaja profiil on arvutatud nii:

1. Leitakse kõik tema peatükkide vaatamised, nende peatükkide teosed ja teoste õppeained - nii on igale peatükile saadud vasteks üks või enam õppeainet.
2. Õpetaja profiiliks on määratud õppeaine (täpsemalt õppeaine võti, st `KitSubjects` veeru `Key` väärtus), mille peatükke on vaadatud kõige rohkem.
3. Nii leitud profiil ei saa õpetajal määramata olla - profiili määramiseks piisab ühest mistahes peatüki vaatamisest, kui ühtegi peatükki pole vaadatud, siis pole ka õpetajale vastavat rida siin tabelis.

Veergude kirjeldused:

1. ...
2. `TeacherProfile` - õpetaja profiil
3. ...

Peatüki sisu vaatamiseks mine (õpetajana) aadressile https://www.opiq.ee/kit/{kitId}/chapter/{ChapterId}, näiteks https://www.opiq.ee/kit/71/chapter/5571 .

## Sessions

Nädalate ja koolide kaupa sessioonide arvud.

Veergude kirjeldused:

1. `IsoWeek` - ISO nädala number
1. `School` - kooli nimi
1. `SchoolCode` - kooli EHISe kood
1. `Teacher` - õpetajate sessioonide arv
1. `GradeUnknown` - selliste õpilaste sessioonide arv, kelle klassi ei õnnestunud arvutada
1. `Grade1` - 1. klassi õpilaste sessioonide arv
1. ...
1. `Grade12` - 12. klassi õpilaste sessioonide arv

## KitSubjects

Teoste õppeained, ühes reas on ühe teose üks õppeaine, teosel võib olla mitu õppeainet, seega võib tabelis olla ühe teose kohta mitu rida.

Veergude kirjeldused:

1. `KitId` - teose võti
2. `Key` - õppeaine võti
3. `Subject` - õppeaine nimetus

## KitClasses

Teoste klassid - klassi number, milles selle õpiku järgi õpitakse. Ühes reas on ühe teose üks klass number, teosel võib olla mitu klassi. `g` tähistab gümnaasiumi.

1. `KitId` - teose võti
2. `ClassNo` - klass
