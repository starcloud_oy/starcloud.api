# Accounting integration

The accounting system must have two endpoints that Opiq calls - `POST /invoice` and `POST /billing/status`.

## POST /invoice

Endpoint provided by accounting system and invoked by OPIQ to transmit invoice data to accounting system.

**Sample input:**

```
{
    "header": {
        "invoiceNo": "A-24-002916",
        "customerId": "343",
        "customerName": "Sample School",
        "registerCode": "123123",
        "customerType": "LegalEntity",
        "docDate": "2024-04-03",
        "dueDate": "2024-04-24",
        "currency": "EUR",
        "paymentAmount": "414.00",
        "paymentAmountWithoutVat": "379.82",
        "vatAmount": "34.18",
        "vatRate": "0.09",
        "vatRateCode": "9-ee",
        "paymentMethod": "Invoice",
        "addressLine1": "Tartu street 40",
        "addressLine2": "Tallinn",
        "addressLine3": "Harjumaa",
        "zipCode": "22211",
        "countryCode": "EE",
        "referenceNumber": "240029168"
    },
    "recipient": {
        "recipientType": "LegalEntity",
        "recipientName": "Tallinna Linnavalitsus",
        "emailAddress": "sekretar@gmail.com",
        "registerCode": "2342345",
        "addressLine1": "Vabaduse väljak 1",
        "addressLine2": "Tallinn",
        "addressLine3": "Harjumaa",
        "zipCode": "33344",
        "countryCode": "EE",
        "vatRegNo": null
    },
    "lines": [
        {
            "packageID": "OPQ10145",
            "packageName": "Õpilase Opiqu ja Foxcademy põhipakett põhikoolile ja gümnaasiumile 2023/24",
            "periodStart": "2024-04-13",
            "periodEnd": "2024-06-12",
            "quantity": "45",
            "paymentAmount": "414.0000",
            "paymentAmountWithoutVat": "379.8165",
            "vatAmount": "34.1835",
            "vatRate": "0.09",
            "vatRateCode": "9-ee"
        }
    ]
}
```

**Explanations:**

1. `header` - in addition to meta data and amounts, the header section contains information about the customer - school or private customer.
1. `invoiceNo` - invoice number, prefix + two last digits of the year + unique number in the year, the prefix is customizable ("A" is the first letter of *arve* - invoice in Estonian)
1. `customerId` - customer's internal ID in Opiq, we can make it contain a shared ID
1. `paymentMethod` - `BankLink`, `Invoice`, `CreditCard`
1. `customerType` - `LegalEntity`, `PrivatePerson`
1. `registerCode` - customer's ID in business register
1. `countryCode` - part of the address
1. `referenceNumber` - reference number on the invoice, consists of numbers in the invoice number
1. `recipient` - the "actual" payer. Typically, the recipient of the goods is the school (customer in the `header` section), but the payer and the individual listed on the invoice (recipient) is the municipal government.
    1. `recipientType` - `LegalEntity`, `PrivatePerson`
    1. `vatRegNo` - VAT registration number
1. `lines` - invoice lines, one line = order of the certain type license.
    1. `packageID` - customizable ID of the product, i.e ID of the license type. Currently is the length limited to 20 symbols.
    1. `periodStart` and `periodEnd` - the start and the end of the license validity
    1. `quantity` - the number of users a license is ordered for
1. `vatRate` - VAT rate in decimal system
1. `vatRateCode` - code assigned to each rate used in the system, can be ignored. Used in Estonia in cases where it is important for accounting to distinguish whether the VAT rate is 0% or the invoice is VAT-free.

**Expected output:** the input JSON + attribute `ACS_ID` in `header` section returning invoice's internal ID in accounting system.

**VAT calculation**

1. First - the total amounts of the invoice lines with VAT are given (the total amount of the invoice = sum of lines).
1. Then the payment amount without VAT is calculated for the lines of the invoice to the nearest four decimal places, rounding is done using the Away-From-Zero method.
1. The VAT amount for the line `=` the total amount of the line `-` payment amount without VAT for the line.
1. After that the payment amount without VAT for the invoice is calculated by summing the invoice line amounts without VAT. The sum is rounded to the two decimal places (Away-From-Zero).
1. The VAT amount for the invoice `=` the total amount of the invoice `-` the payment amount without VAT for the invoice.

## POST /billing/status

Endpoint provided by accounting system and invoked by OPIQ to find out if the invoice has been paid or not. It should be possible to retrieve payment data for multiple (max 50) invoices.

**Sample input:**

```
{
  "invoiceNumbers": [
    "A-22-005700",
    "1256",
  ]
}
```

**Output:**

```
{
    "invoices": [
        {
            "invoiceNumber": "A-22-005700",
            "wasFound": true,
            "invoiceDetails": {
                "paymentAmount": 962.5,
                "paidAmount": 0
            }
        },
        {
            "invoiceNumber": "1256",
            "wasFound": false,
            "invoiceDetails": {
                "paymentAmount": 0,
                "paidAmount": 0
            }
        }
    ]
}
```

**Explanations:**

1. `wasFound` - whether the invoice even exists in the accounting system.
1. `paymentAmount` - the total amount of the invoice as was in the `POST /invoice` request.
1. `paidAmount` - Opiq will store the fact whether the invoice is fully paid or not, even missing 0.01 € will be visible in Opiq as unpaid.

## Authorization

HTTPS request header contains following pre-shared keys and accounting system must check their correctness before accepting the request:

1. `acs-companyCode`
1. `acs-apiKey`
