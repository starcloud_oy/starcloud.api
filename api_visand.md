# Opiqu API visand

Printsiibid:

1. Järgitakse REST api printsiipe.
1. Andmed Json formaadis.
1. Täpne API kirjeldus kaasneb koos API endaga.

## Üldotstarbeline materjali otsing

Leiab materjali, mida saavad kasutada kõik kasutajad: õpetaja, õpilane ja erakasutaja. Välja jäetakse ülesannetekogu ülesanded ja õpetajaraamatu sektsioonid.

1. Endpoint `/api/kits/searchsections`
1. **Sisend**:
    1. **HarId id_token** kuskil päringu päises, st kasutaja tehakse kindlaks
    1. `locale` - üks valik, edastatakse kuskil päringu päises, ei ole kohustuslik. Õppeainete, klasside, õppekavade jms tekstid tagastatakse selles keeles, kui parameeter on esitamata, siis loetakse keeleks Opiqu vaikekeel, Eestis "et".
    1. `searchString` - string, kohustuslik
    1. `curriculums` - õppekavad. Ei ole kohustuslik, multivalik, valikute vahel rakendatakse operaatorit `või`, võimalikud valikud saab päringuga `/api/curriculums` ("GeneralEducation", "GeneralEducationSimplified"). Soovitame vaikimisi kasutajatel otsida valiku `GeneralEducation` järgi. 
    1. `classes` -  klassid. Ei ole kohustuslik, multivalik, valikute vahel rakendatakse operaatorit `või`, võimalikud valikud saab päringuga `/api/classes` ("PreSchool", "1", "2", "3", "4", "5", "6", "7", "8", "9", "HighScool").
    1. `subjects` - õppeained. Ei ole kohustuslik, multivalik, valikute vahel rakendatakse operaatorit `või`, võimalikud valikud saab päringuga `/api/subjects` ("Art", "Biology", "Chemistry", "CivicEducation", ...).
1. **Tulemus** - vektor järgmise struktuuriga objektidega:
    1. `url` - sektsiooni aadress, näiteks "https://www.opiq.ee/kit/39/chapter/1845#s12773".
    1. `kit` - teose andmed:
        1. `id` - teose võti, näit 39
        1. `title` - teose pealkiri, näiteks "Lähiajalugu. Ajalugu 9. klassile"
    1. `chapter` - peatüki andmed:
        1. `id` - peatüki võti, näit 1845
        1. `title` - peatüki pealkiri, näiteks "Pariisi rahu­konverents ja selle otsused"
    1. `section` - sektsiooni andmed:
        1. `id` - sektsiooni võti, näiteks 12773
        1. `title` - sektsiooni pealkiri, näiteks "LISA. Suur nelik Pariisi rahu­konverentsil"
    1. `curriculums` - õppekavad, list järgmise struktuuriga objektidest:
        1. `code` - kood, näiteks "GeneralEducation"
        1. `name` - inimkeelne nimetus, nimetuse keelsus sõltub parameetrist `locale`, näit `locale` = "et" puhul "Üldhariduse riiklik õppekava"
    1. `classes` - klassid, list järgmise struktuuriga objektidest:
        1. `code` - kood, näiteks "9"
        1. `name` - inimkeelne nimetus, keelsus sõltub `locale`'st, näit väärtus "PreSchool" oleks `locale` = "et" puhul "Eelkool"
    1. `subjects` - õppeained, list järgmise struktuuriga objektidest:
        1. `code` - kood, näiteks "History"
        1. `name` - inimkeelne nimetus, keelsus sõltub `locale`'st, näit "Ajalugu"
    1. `thumbnailUrl` - suuruses 175x175 pikslit pisipildi aadress, näiteks "https://astrablobs.blob.core.windows.net/kitcontent/1588fbac-b230-405a-9cff-1c4b417441be/20af3b2f-51de-47b5-b55e-b5e8dc0aefe3/6abbc5af-6b35-448e-9023-b2727b95b955_xssq.jpg".

## Seotud materjali otsing

Leiab sisendis edastatud sektsiooniga seotud materjali, leitu on jaotatud kolme kategooriasse: seotud materjal samas aines, kuid madalamate klasside õppematerjali hulgast - idee järgi sama teema lihtsam käsitlus; seotud materjal samas aines, kuid kõrgemate klasside õppematerjali hulgast - teema keerulisem ja põhjalikum käsitlus; teema käsitlus teistes õppeainetes.

1. Endpoint `/api/kits/x/chapters/y/sections/z/relatedmaterials`, kus **sisendiks** on:
    1. **HarId id_token** kuskil päringu päises, st kasutaja tehakse kindlaks
    1. `locale` - üks valik, edastatakse kuskil päringu päises, ei ole kohustuslik. Õppeainete, klasside, õppekavade jms tekstid tagastatakse selles keeles, kui parameeter on esitamata, siis loetakse keeleks Opiqu vaikekeel, Eestis "et".
    1. `x` - teose võti, näiteks 39
    1. `y` - peatüki võti, näiteks 1845
    1. `z` - sektsiooni võti, näiteks 12773
1. **Tulemus**:
    1. `beforeInSameSubjects` - list seotud materjalist samas aines, kuid varasemates klassides, objektid täpselt sama stuktuuriga, nagu `/api/kits/searchsections` päringu vastuses
    1. `afterInSameSubjects` - list seotud materjalist samas aines, kuid kõrgemates klassides, objektid täpselt sama stuktuuriga, nagu `/api/kits/searchsections` päringu vastuses
    1. `inOtherContent` - list seotud materjalist teistes ainetes, objekti täpselt sama stuktuuriga, nagu `/api/kits/searchsections` päringu vastuses

## Sektsiooni endpoint

Sektsiooni endpoindid, millele kasutaja saab sirvijas minna, on kujul [https://www.opiq.ee/kit/39/chapter/1845#s12773](https://www.opiq.ee/kit/39/chapter/1845#s12773), sinna tuleb päringu päises kaasa panna HarId id_token.

## Konfiguratsiooni päringud

Päringud, mis tagastavad valikud, mida saab otsingutes kasutada, sisend puudub, kasutaja ennast autoriseerima ei pea.

1. `/api/locales` - tagastab süsteemi toetatud lokaalide listi, st tagastab vastuse a'la `locales: ['et', 'ru', 'en', 'fi']`.
1. `/api/curriculums` - tagastab õppekavade loetelu ("GeneralEducation", "GeneralEducationSimplified"), iga õppekava kohta objekti atribuutidega:
    1. `code` - n "GeneralEducation"
    1. `names` - õppekavade nimetused erinevates süsteemis toetatud keeltes, list objektides:
        1. `locale` - n "et"
        1. `name` - n "Üldhariduse riiklik õppekava"
1. `/api/classes` - tagastab klasside (ja pseudoklasside) loetelu ("PreSchool", "1", "2", "3", "4", "5", "6", "7", "8", "9", "HighScool"), iga klassi kohta objekti atribuutidega:
    1. `code` - n "PreSchool"
    1. `names` - klassi nimetused erinevates süsteemis toetatud keeltes, list objektides:
        1. `locale` - n "et"
        1. `name` - n "Eelkool"
1. `/api/subjects` - tagastab õppeainete loetelu ("Art", "Biology", "Chemistry", "CivicEducation", ...), iga õppeaine kohta objekt atribuutidega:
    1. `code` - n "Art"
    1. `names` - klassi nimetused erinevates süsteemis toetatud keeltes, list objektides:
        1. `locale` - n "et"
        1. `name` - n "Kunstiõpetus"