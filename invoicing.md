# Opiq interface for invoicing

## Introduction

Document at hand is intended to give an overview on system interfacing between Opiq and Accounting Software. This enables for an Accounting Software to receive Opiq invoices and provide corresponding payment information.

Henceforth the following content will briefly describe how Opiq works in this aspect and how to achieve a proper communication between two systems.

## Ordering and invoice generation in Opiq

To access study materials in Opiq user (student) must have a valid license. When making an order it can be seen that licence belongs to a package, which specifies some parameters about it - for example license unit cost (per one student, per one month). Order contains one or multiple license(s) which enables user(s) to access these materials for a licensed period.

Regarding payments user has one of two options which influences how invoice(s) will be generated later:

1. One-time payment - it means full payment.
2. Monthly payment - it means multiple payments and a payment schedule. Monthly payment is available only for schools.

Except for when ordering with a gift card, payment schedule is created containing one or multiple scheduled payment(s). Based on payments in that schedule single or multiple invoices are generated (every scheduled payment is placed into one invoice). If full payment is made, invoice is generated directly after order has been confirmed. If a monthly payment option is chosen then invoice is generated according to the date in payment schedule, monthly invoices are generated one day before invoice date (this is made to compensate possible delays between the systems).

Invoice payment deadline is by default 21 days for schools and by default 2 days for private users from invoice date (if invoice date is 1., then deadline is 22.). The interval between invoice date and deadline is configurable for the Opiq system instance by configuration parameters *OrderSchoolPaymentPeriodInDays* for schools and *OrderPrivateUserPaymentPeriodInDays* for private users.

If one customer has multiple orders with multiple scheduled payments and payments from different orders have the same invoice date and deadline then these payments are aggregated to one invoice.

- Order total is not distributed evenly between invoices, but according to the calculations made in payment schedule.
- Order has a scheduled payment day - number of a day in month, a deadline for paying the monthly payment.
- Invoice is numbered in a following format: "A-yy-xxxxxx", where yy is a two-digit year number and xxxxxx six-digit unique number in this year.

**Generated invoices must be delivered to accounting software which deals with getting these to the client and managing payments.**



## Proposal for communication between Opiq and Accounting Software

Communication between Opiq and Accounting Software is initiated by Accounting Software. Accounting Software uses services provided by Opiq to retrieve invoice data and pass corresponding payment information to Opiq.

Every instance of Opiq has its own root address for billing services - for example `https://opiq.ee/billing-api/`, `https://opiq.kz/billing-api/`.

Endpoints are accessible over public internet.

HTTPS and Basic Authorization need to be used to secure the connection between Accounting Software and Opiq.

Sequence diagram describing use of services:
![](invoicing.png)

Diagram describing invoice state transitions:
![](invoicing_states.png)

### Service which Opiq provides to retrieve invoice data

1. **Endpoint name: `GetNewInvoices`.** Address example https://opiq.ee/billing-api/GetNewInvoices.
2. **Use:** Accounting Software makes HTTPS GET request to this endpoint with no query parameter. Opiq will return invoices, which have in Opiq state `New`.
3. **Response from Opiq:** HTTP return code 200 and following result in JSON format is returned:  
```
{
	"invoices":
        [
            {
                "header":
                    {
                        "invoiceNo":"A-19-000065",
                        "customerID":"10400025",
                        "customerName":"Some School",
                        "customerEmail":"info@someschool.com",
                        "docDate":"2016-12-01",
                        "dueDate":"2016-12-22",
                        "paymentAmount":"91.35",
                        "paymentMethod":"Invoice"
                    },
                "lines":
                    [
                        {
                            "packageID":"0100010102",
                            "packageName":"Students Basic",
                            "periodStart":"2016-12-22",
                            "periodEnd":"2017-02-25",
                            "quantity":"2",
                            "paymentAmount":"44.2"
                        },
                        {
                            "packageID":"0100010103",
                            "packageName":"Students Extra",
                            "periodStart":"2016-12-22",
                            "periodEnd":"2017-03-15",
                            "quantity":"1",
                            "paymentAmount":"47.15"
                        }
                    ]
            },
            {
                "header":
                    {
						"invoiceNo":"A-19-0000066",
                        "customerID":"10400022",
                        "customerName":"Another customer",
                      	"customerEmail":"info@anothercustomer.com",
						"docDate":"2016-12-01",
                        "dueDate":"2016-12-22",
                        "paymentAmount":"44.2",
                        "paymentMethod":"Invoice"
                    },
                "lines":
                    [
                        {
                            "packageID":"0100010102",
                            "packageName":"Students Basic",
                            "periodStart":"2016-12-22",
                            "periodEnd":"2017-02-25",
                            "quantity":"2",
                            "paymentAmount":"44.2"
                        }
                    ]
            }
        ],
    "returnedResultCount":"2",
    "hasMoreResults":false,
	"totalResultCount":"2"
}
```
4. If there are no new invoices, then HTTP response code 200 and following JSON is returned:  
```
{
	"invoices":
        [],
    "returnedResultCount":"0",
    "hasMoreResults":false,
	"totalResultCount":"0"
}
```
5. Description of fields:
    1. `customerID` - customer's or private user's customer code can be used for identifying customer in Accounting Software. In Estonia schools have unique codes, but private users share code "00000004". Alternative solutions can be considered. Customer id can be assigned to school in Opiq with customer management module.
    1. `customerEmail` - customer's email address, can be used to send invoice.
    1. `docDate` - invoice date.
    1. `paymentMethod`:
        1. `BankLink` - if invoice is paid via bank link together with the creation of order. It assumes special development for each country, currently implemented in Estonia for local banks.
        1. `Invoice` - if invoice must be delivered to customer and customer pays via bank transfer.
    1. `packageId` - package id or code equals product code in Accounting Software. These codes can and should be synchronized between Opiq and Accounting Software.
    1. One item in `lines` = one invoice line = payment for one license.
    1. `periodStart` - start of the license payment period.
    1. `periodEnd`- end of the license payment period.
    1. `quantity` - quantity of licenses. For example, 2 in first line of the example before means that 2 students can access study materials for two months (starting from 2016-12-22 until 2017-02-25).
    1. Header's `paymentAmount` - total amount of invoice including VAT.
    1. Line's `paymentAmount` - total amount of this line including VAT. PaymentAmount is basically = quantity * period length in months * license unit price for one month for one user.
    1. `ReturnedResultCount` - count of invoices returned by the request.
    1. `HasMoreResults` - the maximum count of invoices returned by the request is limited, `HasMoreResults` is `true` when there is more new invoices in Opiq. If there is more new invoices in Opiq then invoices returned by the request must be processed and marked as `Delivered_to_Accounting_Software` in Opiq and after that the next batch must be requested.
	1. `TotalResultCount` - count of invoices returned by the request + count of invoices left in Opiq.
6. Notes
	1. After saving invoices to its database Accounting Software must use service `MarkInvoicesDelivered` to inform Opiq about transfer of invoices. These invoices will be marked in Opiq as `Delivered_to_Accounting_Software` and will not be returned next time `GetNewInvoices` is used.
	1. Accounting system must wait for invoice date before sending invoice to customer.
	1. Scheduled payments for one client with the same invoice day and deadline are aggregated to one invoice.
    1. All dates are in YYYY-MM-DD format. All amounts use period as a decimal separator (e.g 44.15)

### Service which Opiq provides to mark invoices delivered to Accounting System

Accounting Software must inform Opiq about invoices that are successfully saved to (Accounting Software) database. These invoices will be marked in Opiq as `Delivered_to_Accounting_Software` and will be not returned next time `GetNewInvoices` is used.

1. **Endpoint name: `MarkInvoicesDelivered`.** Address example https://opiq.ee/billing-api/MarkInvoicesDelivered.
2. **Use:** Accounting Software makes HTTPS PUT request to this endpoint with following message in JSON format:  
```
{
    "invoiceNumbers":
        [
            "A-19-0000065",
            "A-19-0000066"
        ]
```
3. If the sent JSON is OK, then HTTP return code 200 and following JSON is returned:  
```
{
    "status":true
}
```
4. If not, then `status` in JSON equals `false` and attribute `error` contains the description for error. For example when performing a GET to the url, then following JSON is returned: 
```
{
    "status":false,
    "error":"Unknown method"
}
```

### Service which Opiq provides to mark invoices paid

Accounting Service must use this service to inform Opiq abount payments. 

1. **Endpoint name: `MarkInvoicesPaid`.** Address example https://opiq.ee/billing-app/MarkInvoicesPaid.
2. **Use**: Accounting Software makes HTTPS PUT request to this endpoint with following message in JSON format:  
```
{
    "invoices":
        [
            {
                "invoiceNumber": "A-16-000013",
                "invoiceDetails":
                {
                    "isPaid": true,
                    "paidDate": "2017-03-15"
                }
            },
            {
                "invoiceNumber": "A-16-000012",
                "invoiceDetails":
                {
                    "isPaid": false
                }
            }
        ]
}
```
3. Description of fields:
	1. `isPaid` - `true` means that invoice is paid, `false` means that payment was revoked.
    1. `paidDate`- date when payment was made. If `isPaid` is `false` then attribute `paidDate` is not present in response.
4. If the sent JSON is OK, then HTTP return code 200 and following JSON is returned:  
```
{
    "status":true
}
```
5. If not, then `status` in JSON equals `false` and attribute `error` contains the description for error. For example when performing a GET to the url, then following JSON is returned: 
```
{
    "status":false,
    "error":"Unknown method"
}
```